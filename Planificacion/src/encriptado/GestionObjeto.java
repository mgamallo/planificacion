package encriptado;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class GestionObjeto {

	final static String NOMBRE_FICHERO = "config.dat";
	
	public static boolean serializaObjeto(Conexion conexion){
		
		boolean serializado = true;
		
		try {
			FileOutputStream fos = new FileOutputStream(NOMBRE_FICHERO);
			ObjectOutputStream salida = new ObjectOutputStream(fos);
			salida.writeObject(conexion);
			salida.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			serializado = false;
			return serializado;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return serializado;
	}
	
	static public Conexion deserializaObjeto(){

		Conexion conexion = null;
		
		try {
			FileInputStream is = new FileInputStream(NOMBRE_FICHERO);
			ObjectInput oi = new ObjectInputStream(is);
			conexion = (Conexion) oi.readObject();
			oi.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Fichero no encontrado");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error de entrada salida");
		} catch (ClassNotFoundException e) {
			System.out.println("Error Clasnotfoundexception");
			e.printStackTrace();
		}

		/*
		System.out.println("Servidor: " + conexion.getNombreServidor());
		System.out.println("ConsultaSQL: " + conexion.getConsultaSQL());
		System.out.println("Base de datos: " + conexion.getNombreBD());
		System.out.println("Password: " + conexion.getClave());
		System.out.println("Usuario: " + conexion.getNombreUsuario());
		 */

		
		return conexion;
	}
}
