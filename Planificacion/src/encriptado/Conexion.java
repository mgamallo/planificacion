package encriptado;

import java.io.Serializable;

public class Conexion implements Serializable{

	private static final long serialVersionUID = 7461074585952949848L;
	
	private String nombreServidor;
	private String nombreBD;
	private String nombreUsuario;
	private String clave;
	private String consultaSQL;
	
	public Conexion(String nombreServidor, String nombreBD,
			String nombreUsuario, String clave, String consultaSQL) throws Exception {
		super();
		this.nombreServidor = Crypto.encrypt(nombreServidor);
		this.nombreBD = Crypto.encrypt(nombreBD);
		this.nombreUsuario = Crypto.encrypt(nombreUsuario);
		this.clave = Crypto.encrypt(clave);
		this.consultaSQL = Crypto.encrypt(consultaSQL);
	}

	public boolean desEncriptaObjeto(){
		boolean desencriptado = true;
		
		try {
			this.nombreServidor = Crypto.decrypt(nombreServidor);
			this.nombreBD = Crypto.decrypt(nombreBD);
			this.nombreUsuario = Crypto.decrypt(nombreUsuario);
			this.clave = Crypto.decrypt(clave);
			this.consultaSQL = Crypto.decrypt(consultaSQL);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return false;
		}
		
		return desencriptado;
	}
	
	public String getNombreServidor() {
		return nombreServidor;
	}

	public void setNombreServidor(String nombreServidor) {
		this.nombreServidor = nombreServidor;
	}

	public String getNombreBD() {
		return nombreBD;
	}

	public void setNombreBD(String nombreBD) {
		this.nombreBD = nombreBD;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}


	public String getConsultaSQL() {
		return consultaSQL;
	}


	public void setConsultaSQL(String consultaSQL) {
		this.consultaSQL = consultaSQL;
	}
}
