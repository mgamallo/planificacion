package encriptado;

/**
 * Clase que consume los métodos de la clase StringEncrypt
 * @author Julio Chinchilla
 */
public class Main {
 
	public static void main(String[] args) {
		String key = "92AE31A79FEEB2A3"; // llave
		String iv = "0123456789ABCDEF"; // vector de inicialización
		String cleartext = "hola";
		try {
			System.out.println("Texto encriptado: "
					+ StringEncrypt.encrypt(key, iv, cleartext));
			System.out.println("Texto desencriptado: "
					+ StringEncrypt.decrypt(key, iv,
							StringEncrypt.encrypt(key, iv, cleartext)));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
 
}