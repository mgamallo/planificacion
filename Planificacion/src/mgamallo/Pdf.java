package mgamallo;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Calendar;

import javax.swing.JOptionPane;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

public class Pdf {

	public static int numeroTotalColumnas = 2+ Planificacion.horas.length + 1;
	public String nombreFichero = "";
	
	private Planificacion planificacion;
	private boolean ficheroCreado = false;
	
	

	public Pdf(Planificacion planificacion) {

		nombreFichero = generarNombreFichero();
		
		this.planificacion = planificacion;
		try {
			/* horizontal */
			// ficheroCreado = generarPdf();
			/* vertical */
			ficheroCreado = generarPdfVertical();
			// ficheroCreado = generarPdfVerticalSalto();

		} catch (FileNotFoundException | DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String getNombreFichero() {
		return nombreFichero;
	}

	private String generarNombreFichero() {
		// TODO Auto-generated method stub
		Calendar cal = Calendar.getInstance();
		
		String nombreFichero = "" + cal.get(Calendar.YEAR);
		
		int mes = cal.get(Calendar.MONTH) + 1;
		if(mes < 10){
			nombreFichero += "0";
		}
		
		nombreFichero += mes;
		int dia = cal.get(Calendar.DAY_OF_MONTH);
		if(dia < 10){
			nombreFichero += "0";
		}
		nombreFichero += dia;
		nombreFichero += "_";
				
		int hora = cal.get(Calendar.HOUR_OF_DAY);
		if(hora < 10){
			nombreFichero += "0";
		}
		nombreFichero += hora;
		
		int minutos = cal.get(Calendar.MINUTE);
		if(minutos < 10){
			nombreFichero += "0";
		}
		nombreFichero += minutos;
		
		int segundos = cal.get(Calendar.SECOND);
		if(segundos < 10){
			nombreFichero += "0";
		}
		nombreFichero += segundos;
		
		System.out.println(nombreFichero);
		return nombreFichero;
	}

	
	private boolean generarPdfVerticalSalto() throws FileNotFoundException,
			DocumentException {
		int numPagina = 0;

		Document documento = new Document(PageSize.A4, 20, 20, 20, 40);
		// new Document(PageSize.A4, 36, 72, 108, 108);
		PdfWriter writer = null;
		boolean ficheroAbierto = true;
		

		
		try {
			writer = PdfWriter.getInstance(documento, new FileOutputStream(
					"pdfs/"+ nombreFichero + ".pdf"));
			ficheroAbierto = false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Cierra los archivos pdf abiertos, y vuelve a intentarlo.");
		}
		if (!ficheroAbierto) {
			writer.setInitialLeading(10);

			PiePagina pie = new PiePagina();
			writer.setPageEvent(pie);

			documento.open();

			Calendar cal1 = Calendar.getInstance();

			String fecha = ""
					+ planificacion.getFechaEntrega().get(Calendar.DATE) + "/"
					+ (planificacion.getFechaEntrega().get(Calendar.MONTH) + 1)
					+ "/" + planificacion.getFechaEntrega().get(Calendar.YEAR);

			Font fuente1 = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD);
			Font fuente2 = new Font(FontFamily.TIMES_ROMAN, 9, Font.ITALIC);
			Font fuente3 = new Font(FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			Font fuente3N = new Font(FontFamily.TIMES_ROMAN, 9, Font.BOLD);
			Font fuente4 = new Font(FontFamily.TIMES_ROMAN, 7, Font.NORMAL);
			Font saltolinea = new Font(FontFamily.TIMES_ROMAN, 6, Font.NORMAL);

			documento.add(new Paragraph("PLANIFICACI�N ENTREGA RESERVAS",
					fuente1));
			documento.add(new Paragraph(4f, " ", saltolinea));
			documento
					.add(new Paragraph(10f, "FECHA ENTREGA: " + fecha, fuente2));
			documento.add(new Paragraph(10f, "ARTICULO: Todos", fuente2));
			documento.add(new Paragraph(10f, "ESTADOS RESERVA: Reservado",
					fuente2));
			documento.add(new Paragraph(" "));

			int numeroTotalColumnas = 0;
			int numeroColumnasDescripcion = 2;
			int numeroColumnasHoras = Planificacion.horas.length + 1;

			System.out.println("Tama�o tabla planificacion: "
					+ Planificacion.horas.length);

			numeroTotalColumnas = numeroColumnasDescripcion
					+ numeroColumnasHoras;

			PdfPTable tabla = new PdfPTable(numeroTotalColumnas);
			tabla.setWidthPercentage(100);
			tabla.setHeaderRows(1);

			int numColumnas = 2 + Planificacion.horas.length + 1;
			String encabezados[] = new String[numColumnas];

			float anchoColumnas[] = new float[numColumnas];

			encabezados[0] = "C�digo";
			anchoColumnas[0] = 1.5f;
			encabezados[1] = "Descripci�n";
			anchoColumnas[1] = 4f;
			for (int i = 0; i < Planificacion.horas.length; i++) {
				encabezados[i + 2] = Planificacion.horas[i];
				anchoColumnas[i + 2] = 1f;
			}
			encabezados[numColumnas - 1] = "Total";
			anchoColumnas[numColumnas - 1] = 1f;

			tabla.setWidths(anchoColumnas);

			PdfPCell celdas[] = new PdfPCell[numColumnas];
			for (int i = 0; i < numColumnas; i++) {
				Paragraph contenido = new Paragraph();
				contenido.setFont(fuente3N);
				contenido.add(encabezados[i]);
				celdas[i] = new PdfPCell(contenido);
				celdas[i].setHorizontalAlignment(Element.ALIGN_CENTER);
				tabla.addCell(celdas[i]);
			}

			Paragraph cont = new Paragraph();
			cont.add(" ");
			PdfPCell cel = new PdfPCell(cont);
			cel.setColspan(numColumnas);
			cel.setBorder(Rectangle.NO_BORDER);
			tabla.addCell(cel);

			tabla = iniciaTabla(documento, numeroTotalColumnas, tabla,
					anchoColumnas);

			for (int b = 0; b < planificacion.getListaSecciones().size(); b++) {
				Seccion seccion = planificacion.getListaSecciones().get(b);

				if (b != 0) {
					tabla = iniciaTabla(documento, numeroTotalColumnas, tabla,
							anchoColumnas);
				}

				cont = new Paragraph();
				cont.add(" ");
				cel = new PdfPCell(cont);
				cel.setColspan(numColumnas);
				cel.setBorder(Rectangle.NO_BORDER);
				tabla.addCell(cel);

				// Encabezado secci�n
				Paragraph contenido = new Paragraph();
				contenido.setFont(fuente3N);
				contenido.add("Sec.: " + seccion.getCodigo());
				PdfPCell celdaSeccion = new PdfPCell(contenido);
				celdaSeccion.setBorder(Rectangle.NO_BORDER);
				celdaSeccion.setHorizontalAlignment(Element.ALIGN_RIGHT);
				tabla.addCell(celdaSeccion);

				contenido = new Paragraph();
				contenido.setFont(fuente3N);
				contenido.add(seccion.getNombre());
				celdaSeccion = new PdfPCell(contenido);
				celdaSeccion.setBorder(Rectangle.NO_BORDER);
				tabla.addCell(celdaSeccion);

				for (int ix = 2; ix < numColumnas; ix++) {
					tabla.addCell(celdas[ix]);
				}

				// Agregado por agregado
				for (int j = 0; j < seccion.getListaAgregados().size(); j++) {

					Agregado agregado = seccion.getListaAgregados().get(j);

					contenido = new Paragraph();
					contenido.setFont(fuente3);
					contenido.add(""
							+ agregado.getArticuloAgregado().getCodArticulo());
					celdaSeccion = new PdfPCell(contenido);
					celdaSeccion.setHorizontalAlignment(Element.ALIGN_RIGHT);
					tabla.addCell(celdaSeccion);

					contenido = new Paragraph();
					contenido.setFont(fuente3);
					contenido.add(""
							+ agregado.getArticuloAgregado().getDescripcion());
					celdaSeccion = new PdfPCell(contenido);
					celdaSeccion.setHorizontalAlignment(Element.ALIGN_LEFT);
					tabla.addCell(celdaSeccion);

					for (int z = 0; z < numColumnas - 3; z++) {
						int contenidoCelda = seccion.getTablaContadora()[j][z];
						if (contenidoCelda > 0) {
							contenido = new Paragraph();
							contenido.setFont(fuente3);
							contenido.add("" + contenidoCelda);
							celdaSeccion = new PdfPCell(contenido);
							celdaSeccion
									.setHorizontalAlignment(Element.ALIGN_CENTER);
							tabla.addCell(celdaSeccion);
						} else {
							tabla.addCell(" ");
						}

					}

					contenido = new Paragraph();
					contenido.setFont(fuente3N);
					contenido.add("" + agregado.getSumaArticulos());
					celdaSeccion = new PdfPCell(contenido);
					celdaSeccion.setHorizontalAlignment(Element.ALIGN_RIGHT);
					tabla.addCell(celdaSeccion);

					// Modificadores de cada agregado
					for (int z = 0; z < agregado.getArticuloAgregado()
							.getListaModificadores().size(); z++) {
						String modificador = agregado.getArticuloAgregado()
								.getListaModificadores().get(z);

						// documento.add(new Paragraph(4f, " ", saltolinea));

						float altoFila = 11f;

						contenido = new Paragraph();
						contenido.setFont(fuente4);
						celdaSeccion = new PdfPCell(contenido);
						celdaSeccion.setBorder(Rectangle.LEFT);
						celdaSeccion.setFixedHeight(altoFila);
						tabla.addCell(celdaSeccion);

						contenido = new Paragraph();
						contenido.setFont(fuente4);
						contenido.add("" + modificador);
						celdaSeccion = new PdfPCell(contenido);
						celdaSeccion
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						celdaSeccion.setBorder(Rectangle.NO_BORDER);
						celdaSeccion.setFixedHeight(altoFila);
						tabla.addCell(celdaSeccion);

						celdaSeccion = new PdfPCell();
						contenido.setFont(fuente4);
						celdaSeccion.setBorder(Rectangle.LEFT);
						celdaSeccion.setFixedHeight(altoFila);
						tabla.addCell(celdaSeccion);

						celdaSeccion = new PdfPCell();
						contenido.setFont(fuente4);
						celdaSeccion.setColspan(11);
						celdaSeccion.setBorder(Rectangle.NO_BORDER);
						celdaSeccion.setFixedHeight(altoFila);
						tabla.addCell(celdaSeccion);

						celdaSeccion = new PdfPCell();
						contenido.setFont(fuente4);
						celdaSeccion.setBorder(Rectangle.RIGHT);
						celdaSeccion.setFixedHeight(altoFila);
						tabla.addCell(celdaSeccion);

					}

					if (j != seccion.getListaAgregados().size() - 1) {
						tabla = iniciaTabla(documento, numeroTotalColumnas,
								tabla, anchoColumnas);
					}
					
				}

				Paragraph sumatorios = new Paragraph(" ");
				celdaSeccion = new PdfPCell(sumatorios);
				celdaSeccion.setColspan(2);
				celdaSeccion.setBorder(Rectangle.TOP);
				tabla.addCell(celdaSeccion);

				int sumaSeccion = 0;
				for (int k = 0; k < numColumnas - 2; k++) {

					int suma = 0;
					for (int s = 0; s < seccion.getTablaContadora().length
							&& k < numColumnas - 3; s++) {
						// System.out.println(s + "\t" + k);
						suma += seccion.getTablaContadora()[s][k];
					}

					sumaSeccion += suma;

					if (suma > 0) {
						String sum = "" + suma;
						sumatorios = new Paragraph();
						sumatorios.setFont(fuente3N);
						sumatorios.add(sum);
					} else {
						sumatorios = new Paragraph(" ");
					}

					celdaSeccion = new PdfPCell(sumatorios);
					celdaSeccion.setHorizontalAlignment(Element.ALIGN_CENTER);

					if (k == numColumnas - 2 - 1) {
						String sum = "" + sumaSeccion;
						sumatorios = new Paragraph("");
						sumatorios.setFont(fuente3N);
						sumatorios.add(sum);
						celdaSeccion = new PdfPCell(sumatorios);
						celdaSeccion
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
					}

					celdaSeccion.setBorder(Rectangle.BOX);
					tabla.addCell(celdaSeccion);

					// tabla = iniciaTabla(documento, numeroTotalColumnas,
					// tabla, anchoColumnas);
				}

			}

			documento.add(tabla);

			documento.close();

			return true;
		}

		return false;
	}

	private boolean generarPdfVertical() throws FileNotFoundException,
			DocumentException {
		int numPagina = 0;

		// float altoPaginaUtil = 148f;
		float altoPaginaUtil = 222f; // 220f

		float altoArtiSecc = 6.2f;
		float altoModif = 3.5f;
		float sumaActualAlto = 0f;

		int numArtiSecc = 0;
		int numModif = 0;

		Document documento = new Document(PageSize.A4, 20, 20, 20, 40);
		// new Document(PageSize.A4, 36, 72, 108, 108);
		PdfWriter writer = null;
		boolean ficheroAbierto = true;
		try {
			writer = PdfWriter.getInstance(documento, new FileOutputStream(
					"pdfs/"+ nombreFichero + ".pdf"));
			ficheroAbierto = false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Cierra los archivos pdf abiertos, y vuelve a intentarlo.");
		}
		if (!ficheroAbierto) {
			writer.setInitialLeading(10);

			PiePagina pie = new PiePagina();
			writer.setPageEvent(pie);

			documento.open();

			Calendar cal1 = Calendar.getInstance();

			String fecha = ""
					+ planificacion.getFechaEntrega().get(Calendar.DATE) + "/"
					+ (planificacion.getFechaEntrega().get(Calendar.MONTH) + 1)
					+ "/" + planificacion.getFechaEntrega().get(Calendar.YEAR);

			Font fuente1 = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD);
			Font fuente2 = new Font(FontFamily.TIMES_ROMAN, 9, Font.ITALIC);
			Font fuente3 = new Font(FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			Font fuente3N = new Font(FontFamily.TIMES_ROMAN, 9, Font.BOLD);
			Font fuente4 = new Font(FontFamily.TIMES_ROMAN, 7, Font.NORMAL);
			Font saltolinea = new Font(FontFamily.TIMES_ROMAN, 6, Font.NORMAL);

			documento.add(new Paragraph("PLANIFICACI�N ENTREGA RESERVAS",
					fuente1));
			documento.add(new Paragraph(4f, " ", saltolinea));
			documento
					.add(new Paragraph(10f, "FECHA ENTREGA: " + fecha, fuente2));
			documento.add(new Paragraph(10f, "ARTICULO: Todos", fuente2));
			documento.add(new Paragraph(10f, "ESTADOS RESERVA: Reservado",
					fuente2));
			documento.add(new Paragraph(" "));

			int numeroTotalColumnas = 0;
			int numeroColumnasDescripcion = 2;
			int numeroColumnasHoras = Planificacion.horas.length + 1;

			System.out.println("Tama�o tabla planificacion: "
					+ Planificacion.horas.length);

			numeroTotalColumnas = numeroColumnasDescripcion
					+ numeroColumnasHoras;

			PdfPTable tabla = new PdfPTable(numeroTotalColumnas);
			tabla.setWidthPercentage(100);
			tabla.setHeaderRows(1);

			int numColumnas = 2 + Planificacion.horas.length + 1;
			String encabezados[] = new String[numColumnas];

			float anchoColumnas[] = new float[numColumnas];

			encabezados[0] = "C�digo";
			anchoColumnas[0] = 1.5f;
			encabezados[1] = "Descripci�n";
			anchoColumnas[1] = 4f;
			for (int i = 0; i < Planificacion.horas.length; i++) {
				encabezados[i + 2] = Planificacion.horas[i];
				anchoColumnas[i + 2] = 1f;
			}
			encabezados[numColumnas - 1] = "Total";
			anchoColumnas[numColumnas - 1] = 1f;

			tabla.setWidths(anchoColumnas);

			PdfPCell celdas[] = new PdfPCell[numColumnas];
			for (int i = 0; i < numColumnas; i++) {
				Paragraph contenido = new Paragraph();
				contenido.setFont(fuente3N);
				contenido.add(encabezados[i]);
				celdas[i] = new PdfPCell(contenido);
				celdas[i].setHorizontalAlignment(Element.ALIGN_CENTER);
				tabla.addCell(celdas[i]);
			}

			for (int i = 0; i < planificacion.getListaSecciones().size(); i++) {

				Seccion seccion = planificacion.getListaSecciones().get(i);

				// Encabezado secci�n
				Paragraph contenido = new Paragraph();
				contenido.setFont(fuente3N);
				contenido.add("Sec.: " + seccion.getCodigo());
				PdfPCell celdaSeccion = new PdfPCell(contenido);
				celdaSeccion.setBorder(Rectangle.NO_BORDER);
				celdaSeccion.setHorizontalAlignment(Element.ALIGN_RIGHT);
				tabla.addCell(celdaSeccion);

				contenido = new Paragraph();
				contenido.setFont(fuente3N);
				contenido.add(seccion.getNombre());
				celdaSeccion = new PdfPCell(contenido);
				celdaSeccion.setColspan(14);
				celdaSeccion.setBorder(Rectangle.NO_BORDER);
				tabla.addCell(celdaSeccion);

				numArtiSecc++;

				// Agregado por agregado
				for (int j = 0; j < seccion.getListaAgregados().size(); j++) {

					Agregado agregado = seccion.getListaAgregados().get(j);

					// Comprueba que no va a pasar de p�gina
					// *****************************
					sumaActualAlto = numArtiSecc * altoArtiSecc + numModif
							* altoModif;
					System.out.println("Suma actual: " + sumaActualAlto);
					int numArtiFutur = numArtiSecc + 1;
					int numModifFutur = numModif
							+ agregado.getArticuloAgregado()
									.getListaModificadores().size();

					float sumaFutura = numArtiFutur * altoArtiSecc
							+ numModifFutur * altoModif;

					System.out.println("Suma previsible: " + sumaFutura);

					if (sumaFutura > altoPaginaUtil) {
						sumaActualAlto += altoArtiSecc;
						contenido = new Paragraph();
						contenido.setFont(fuente3N);
						contenido.add(" ");
						celdaSeccion = new PdfPCell(contenido);
						celdaSeccion.setColspan(numeroTotalColumnas);
						celdaSeccion.setBorder(Rectangle.TOP);
						tabla.addCell(celdaSeccion);

						for (int xx = 0; xx < agregado.getArticuloAgregado()
								.getListaModificadores().size() + 1; xx++) {
							sumaActualAlto += altoArtiSecc;
							if (sumaActualAlto < altoPaginaUtil) {
								contenido = new Paragraph();
								contenido.setFont(fuente3N);
								contenido.add(" ");
								celdaSeccion = new PdfPCell(contenido);
								celdaSeccion.setColspan(numeroTotalColumnas);
								celdaSeccion.setBorder(Rectangle.NO_BORDER);
								tabla.addCell(celdaSeccion);
							} else {
								contenido = new Paragraph();
								contenido.setFont(fuente3N);
								contenido.add(" ");
								celdaSeccion = new PdfPCell(contenido);
								celdaSeccion.setColspan(numeroTotalColumnas);
								celdaSeccion.setBorder(Rectangle.NO_BORDER);
								tabla.addCell(celdaSeccion);
								tabla.addCell(celdaSeccion);
								tabla.addCell(celdaSeccion);
								break;
							}
						}

						numPagina++;
						altoPaginaUtil = 222f + 38f;
						numArtiSecc = 0;
						numModif = 0;
						System.out
								.println("******************************Salto de p�gina "
										+ numPagina);
					}

					numArtiSecc++;
					numModif += agregado.getArticuloAgregado()
							.getListaModificadores().size();

					contenido = new Paragraph();
					contenido.setFont(fuente3);
					contenido.add(""
							+ agregado.getArticuloAgregado().getCodArticulo());
					celdaSeccion = new PdfPCell(contenido);
					celdaSeccion.setHorizontalAlignment(Element.ALIGN_RIGHT);
					tabla.addCell(celdaSeccion);

					contenido = new Paragraph();
					contenido.setFont(fuente3);
					contenido.add(""
							+ agregado.getArticuloAgregado().getDescripcion());
					celdaSeccion = new PdfPCell(contenido);
					celdaSeccion.setHorizontalAlignment(Element.ALIGN_LEFT);
					tabla.addCell(celdaSeccion);

					for (int z = 0; z < numColumnas - 3; z++) {
						int contenidoCelda = seccion.getTablaContadora()[j][z];
						if (contenidoCelda > 0) {
							contenido = new Paragraph();
							contenido.setFont(fuente3);
							contenido.add("" + contenidoCelda);
							celdaSeccion = new PdfPCell(contenido);
							celdaSeccion
									.setHorizontalAlignment(Element.ALIGN_CENTER);
							tabla.addCell(celdaSeccion);
						} else {
							tabla.addCell(" ");
						}

					}

					contenido = new Paragraph();
					contenido.setFont(fuente3N);
					contenido.add("" + agregado.getSumaArticulos());
					celdaSeccion = new PdfPCell(contenido);
					celdaSeccion.setHorizontalAlignment(Element.ALIGN_RIGHT);
					tabla.addCell(celdaSeccion);

					// Modificadores de cada agregado
					for (int z = 0; z < agregado.getArticuloAgregado()
							.getListaModificadores().size(); z++) {
						String modificador = agregado.getArticuloAgregado()
								.getListaModificadores().get(z);

						// documento.add(new Paragraph(4f, " ", saltolinea));

						float altoFila = 11f;

						contenido = new Paragraph();
						contenido.setFont(fuente4);
						celdaSeccion = new PdfPCell(contenido);
						celdaSeccion.setBorder(Rectangle.LEFT);
						celdaSeccion.setFixedHeight(altoFila);
						tabla.addCell(celdaSeccion);

						contenido = new Paragraph();
						contenido.setFont(fuente4);
						contenido.add("" + modificador);
						celdaSeccion = new PdfPCell(contenido);
						celdaSeccion
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						celdaSeccion.setBorder(Rectangle.NO_BORDER);
						celdaSeccion.setFixedHeight(altoFila);
						tabla.addCell(celdaSeccion);

						celdaSeccion = new PdfPCell();
						contenido.setFont(fuente4);
						celdaSeccion.setBorder(Rectangle.LEFT);
						celdaSeccion.setFixedHeight(altoFila);
						tabla.addCell(celdaSeccion);

						celdaSeccion = new PdfPCell();
						contenido.setFont(fuente4);
						celdaSeccion.setColspan(11);
						celdaSeccion.setBorder(Rectangle.NO_BORDER);
						celdaSeccion.setFixedHeight(altoFila);
						tabla.addCell(celdaSeccion);

						celdaSeccion = new PdfPCell();
						contenido.setFont(fuente4);
						celdaSeccion.setBorder(Rectangle.RIGHT);
						celdaSeccion.setFixedHeight(altoFila);
						tabla.addCell(celdaSeccion);

					}
				}

				Paragraph sumatorios = new Paragraph(" ");
				celdaSeccion = new PdfPCell(sumatorios);
				celdaSeccion.setColspan(2);
				celdaSeccion.setBorder(Rectangle.TOP);
				tabla.addCell(celdaSeccion);

				int sumaSeccion = 0;
				for (int k = 0; k < numColumnas - 2; k++) {

					int suma = 0;
					for (int s = 0; s < seccion.getTablaContadora().length
							&& k < numColumnas - 3; s++) {
						// System.out.println(s + "\t" + k);
						suma += seccion.getTablaContadora()[s][k];
					}

					sumaSeccion += suma;

					if (suma > 0) {
						String sum = "" + suma;
						sumatorios = new Paragraph();
						sumatorios.setFont(fuente3N);
						sumatorios.add(sum);
					} else {
						sumatorios = new Paragraph(" ");
					}

					celdaSeccion = new PdfPCell(sumatorios);
					celdaSeccion.setHorizontalAlignment(Element.ALIGN_CENTER);

					if (k == numColumnas - 2 - 1) {
						String sum = "" + sumaSeccion;
						sumatorios = new Paragraph("");
						sumatorios.setFont(fuente3N);
						sumatorios.add(sum);
						celdaSeccion = new PdfPCell(sumatorios);
						celdaSeccion
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
					}

					celdaSeccion.setBorder(Rectangle.BOX);
					tabla.addCell(celdaSeccion);
				}

				numArtiSecc++;

			}

			documento.add(tabla);

			documento.close();

			return true;
		}

		return false;
	}

	private boolean generarPdf() throws FileNotFoundException,
			DocumentException {

		int numPagina = 0;

		float altoPaginaUtil = 148f;
		float altoArtiSecc = 6.2f;
		float altoModif = 3.5f;
		float sumaActualAlto = 0f;

		int numArtiSecc = 0;
		int numModif = 0;

		Document documento = new Document(PageSize.A4.rotate(), 20, 20, 20, 40);
		// new Document(PageSize.A4, 36, 72, 108, 108);
		PdfWriter writer = null;
		boolean ficheroAbierto = true;
		try {
			writer = PdfWriter.getInstance(documento, new FileOutputStream(
					"pdfs/pdf0.pdf"));
			ficheroAbierto = false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,
					"Cierra los archivos pdf abiertos, y vuelve a intentarlo.");
		}
		if (!ficheroAbierto) {
			writer.setInitialLeading(10);

			PiePagina pie = new PiePagina();
			writer.setPageEvent(pie);

			documento.open();

			Calendar cal1 = Calendar.getInstance();

			String fecha = ""
					+ planificacion.getFechaEntrega().get(Calendar.DATE) + "/"
					+ (planificacion.getFechaEntrega().get(Calendar.MONTH) + 1)
					+ "/" + planificacion.getFechaEntrega().get(Calendar.YEAR);

			Font fuente1 = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD);
			Font fuente2 = new Font(FontFamily.TIMES_ROMAN, 9, Font.ITALIC);
			Font fuente3 = new Font(FontFamily.TIMES_ROMAN, 9, Font.NORMAL);
			Font fuente3N = new Font(FontFamily.TIMES_ROMAN, 9, Font.BOLD);
			Font fuente4 = new Font(FontFamily.TIMES_ROMAN, 7, Font.NORMAL);
			Font saltolinea = new Font(FontFamily.TIMES_ROMAN, 6, Font.NORMAL);

			documento.add(new Paragraph("PLANIFICACI�N ENTREGA RESERVAS",
					fuente1));
			documento.add(new Paragraph(4f, " ", saltolinea));
			documento
					.add(new Paragraph(10f, "FECHA ENTREGA: " + fecha, fuente2));
			documento.add(new Paragraph(10f, "ARTICULO: Todos", fuente2));
			documento.add(new Paragraph(10f, "ESTADOS RESERVA: Reservado",
					fuente2));
			documento.add(new Paragraph(" "));

			int numeroTotalColumnas = 0;
			int numeroColumnasDescripcion = 2;
			int numeroColumnasHoras = Planificacion.horas.length + 1;

			System.out.println("Tama�o tabla planificacion: "
					+ Planificacion.horas.length);

			numeroTotalColumnas = numeroColumnasDescripcion
					+ numeroColumnasHoras;

			PdfPTable tabla = new PdfPTable(numeroTotalColumnas);
			tabla.setWidthPercentage(100);
			tabla.setHeaderRows(1);

			int numColumnas = 2 + Planificacion.horas.length + 1;
			String encabezados[] = new String[numColumnas];

			float anchoColumnas[] = new float[numColumnas];

			encabezados[0] = "C�digo";
			anchoColumnas[0] = 1.5f;
			encabezados[1] = "Descripci�n";
			anchoColumnas[1] = 4f;
			for (int i = 0; i < Planificacion.horas.length; i++) {
				encabezados[i + 2] = Planificacion.horas[i];
				anchoColumnas[i + 2] = 1f;
			}
			encabezados[numColumnas - 1] = "Total";
			anchoColumnas[numColumnas - 1] = 1f;

			tabla.setWidths(anchoColumnas);

			PdfPCell celdas[] = new PdfPCell[numColumnas];
			for (int i = 0; i < numColumnas; i++) {
				Paragraph contenido = new Paragraph();
				contenido.setFont(fuente3N);
				contenido.add(encabezados[i]);
				celdas[i] = new PdfPCell(contenido);
				celdas[i].setHorizontalAlignment(Element.ALIGN_CENTER);
				tabla.addCell(celdas[i]);
			}

			for (int i = 0; i < planificacion.getListaSecciones().size(); i++) {

				Seccion seccion = planificacion.getListaSecciones().get(i);

				// Encabezado secci�n
				Paragraph contenido = new Paragraph();
				contenido.setFont(fuente3N);
				contenido.add("Secci�n: " + seccion.getCodigo());
				PdfPCell celdaSeccion = new PdfPCell(contenido);
				celdaSeccion.setBorder(Rectangle.NO_BORDER);
				celdaSeccion.setHorizontalAlignment(Element.ALIGN_RIGHT);
				tabla.addCell(celdaSeccion);

				contenido = new Paragraph();
				contenido.setFont(fuente3N);
				contenido.add(seccion.getNombre());
				celdaSeccion = new PdfPCell(contenido);
				celdaSeccion.setColspan(14);
				celdaSeccion.setBorder(Rectangle.NO_BORDER);
				tabla.addCell(celdaSeccion);

				numArtiSecc++;

				// Agregado por agregado
				for (int j = 0; j < seccion.getListaAgregados().size(); j++) {

					Agregado agregado = seccion.getListaAgregados().get(j);

					// Comprueba que no va a pasar de p�gina
					// *****************************
					sumaActualAlto = numArtiSecc * altoArtiSecc + numModif
							* altoModif;
					// System.out.println("Suma actual: " + sumaActualAlto);
					int numArtiFutur = numArtiSecc + 1;
					int numModifFutur = numModif
							+ agregado.getArticuloAgregado()
									.getListaModificadores().size();

					float sumaFutura = numArtiFutur * altoArtiSecc
							+ numModifFutur * altoModif;

					// System.out.println("Suma previsible: " + sumaFutura);

					if (sumaFutura > altoPaginaUtil) {
						sumaActualAlto += altoArtiSecc;
						contenido = new Paragraph();
						contenido.setFont(fuente3N);
						contenido.add(" ");
						celdaSeccion = new PdfPCell(contenido);
						celdaSeccion.setColspan(numeroTotalColumnas);
						celdaSeccion.setBorder(Rectangle.TOP);
						tabla.addCell(celdaSeccion);

						for (int xx = 0; xx < agregado.getArticuloAgregado()
								.getListaModificadores().size() + 1; xx++) {
							sumaActualAlto += altoArtiSecc;
							if (sumaActualAlto < altoPaginaUtil) {
								contenido = new Paragraph();
								contenido.setFont(fuente3N);
								contenido.add(" ");
								celdaSeccion = new PdfPCell(contenido);
								celdaSeccion.setColspan(numeroTotalColumnas);
								celdaSeccion.setBorder(Rectangle.NO_BORDER);
								tabla.addCell(celdaSeccion);
							} else {
								contenido = new Paragraph();
								contenido.setFont(fuente3N);
								contenido.add(" ");
								celdaSeccion = new PdfPCell(contenido);
								celdaSeccion.setColspan(numeroTotalColumnas);
								celdaSeccion.setBorder(Rectangle.NO_BORDER);
								tabla.addCell(celdaSeccion);
								tabla.addCell(celdaSeccion);
								tabla.addCell(celdaSeccion);
								break;
							}
						}

						numPagina++;
						altoPaginaUtil = 162f;
						numArtiSecc = 0;
						numModif = 0;
						System.out
								.println("******************************Salto de p�gina "
										+ numPagina);
					}

					numArtiSecc++;
					numModif += agregado.getArticuloAgregado()
							.getListaModificadores().size();

					contenido = new Paragraph();
					contenido.setFont(fuente3);
					contenido.add(""
							+ agregado.getArticuloAgregado().getCodArticulo());
					celdaSeccion = new PdfPCell(contenido);
					celdaSeccion.setHorizontalAlignment(Element.ALIGN_RIGHT);
					tabla.addCell(celdaSeccion);

					contenido = new Paragraph();
					contenido.setFont(fuente3);
					contenido.add(""
							+ agregado.getArticuloAgregado().getDescripcion());
					celdaSeccion = new PdfPCell(contenido);
					celdaSeccion.setHorizontalAlignment(Element.ALIGN_LEFT);
					tabla.addCell(celdaSeccion);

					for (int z = 0; z < numColumnas - 3; z++) {
						int contenidoCelda = seccion.getTablaContadora()[j][z];
						if (contenidoCelda > 0) {
							contenido = new Paragraph();
							contenido.setFont(fuente3);
							contenido.add("" + contenidoCelda);
							celdaSeccion = new PdfPCell(contenido);
							celdaSeccion
									.setHorizontalAlignment(Element.ALIGN_CENTER);
							tabla.addCell(celdaSeccion);
						} else {
							tabla.addCell(" ");
						}

					}

					contenido = new Paragraph();
					contenido.setFont(fuente3N);
					contenido.add("" + agregado.getSumaArticulos());
					celdaSeccion = new PdfPCell(contenido);
					celdaSeccion.setHorizontalAlignment(Element.ALIGN_RIGHT);
					tabla.addCell(celdaSeccion);

					// Modificadores de cada agregado
					for (int z = 0; z < agregado.getArticuloAgregado()
							.getListaModificadores().size(); z++) {
						String modificador = agregado.getArticuloAgregado()
								.getListaModificadores().get(z);

						// documento.add(new Paragraph(4f, " ", saltolinea));

						float altoFila = 11f;

						contenido = new Paragraph();
						contenido.setFont(fuente4);
						celdaSeccion = new PdfPCell(contenido);
						celdaSeccion.setBorder(Rectangle.LEFT);
						celdaSeccion.setFixedHeight(altoFila);
						tabla.addCell(celdaSeccion);

						contenido = new Paragraph();
						contenido.setFont(fuente4);
						contenido.add("" + modificador);
						celdaSeccion = new PdfPCell(contenido);
						celdaSeccion
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						celdaSeccion.setBorder(Rectangle.NO_BORDER);
						celdaSeccion.setFixedHeight(altoFila);
						tabla.addCell(celdaSeccion);

						celdaSeccion = new PdfPCell();
						contenido.setFont(fuente4);
						celdaSeccion.setBorder(Rectangle.LEFT);
						celdaSeccion.setFixedHeight(altoFila);
						tabla.addCell(celdaSeccion);

						celdaSeccion = new PdfPCell();
						contenido.setFont(fuente4);
						celdaSeccion.setColspan(11);
						celdaSeccion.setBorder(Rectangle.NO_BORDER);
						celdaSeccion.setFixedHeight(altoFila);
						tabla.addCell(celdaSeccion);

						celdaSeccion = new PdfPCell();
						contenido.setFont(fuente4);
						celdaSeccion.setBorder(Rectangle.RIGHT);
						celdaSeccion.setFixedHeight(altoFila);
						tabla.addCell(celdaSeccion);

					}
				}

				Paragraph sumatorios = new Paragraph(" ");
				celdaSeccion = new PdfPCell(sumatorios);
				celdaSeccion.setColspan(2);
				celdaSeccion.setBorder(Rectangle.TOP);
				tabla.addCell(celdaSeccion);

				int sumaSeccion = 0;
				for (int k = 0; k < numColumnas - 2; k++) {

					int suma = 0;
					for (int s = 0; s < seccion.getTablaContadora().length
							&& k < numColumnas - 3; s++) {
						// System.out.println(s + "\t" + k);
						suma += seccion.getTablaContadora()[s][k];
					}

					sumaSeccion += suma;

					if (suma > 0) {
						String sum = "" + suma;
						sumatorios = new Paragraph();
						sumatorios.setFont(fuente3N);
						sumatorios.add(sum);
					} else {
						sumatorios = new Paragraph(" ");
					}

					celdaSeccion = new PdfPCell(sumatorios);
					celdaSeccion.setHorizontalAlignment(Element.ALIGN_CENTER);

					if (k == numColumnas - 2 - 1) {
						String sum = "" + sumaSeccion;
						sumatorios = new Paragraph("");
						sumatorios.setFont(fuente3N);
						sumatorios.add(sum);
						celdaSeccion = new PdfPCell(sumatorios);
						celdaSeccion
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
					}

					celdaSeccion.setBorder(Rectangle.BOX);
					tabla.addCell(celdaSeccion);
				}

				numArtiSecc++;

			}

			documento.add(tabla);

			documento.close();

			return true;
		}

		return false;
	}

	private PdfPTable iniciaTabla(Document documento, int numeroTotalColumnas,
			PdfPTable tabla, float[] anchoColumnas) throws DocumentException {
		documento.add(tabla);
		tabla = new PdfPTable(numeroTotalColumnas);
		tabla.setWidthPercentage(100);
		tabla.setWidths(anchoColumnas);
		tabla.setKeepTogether(true);
		return tabla;
	}

	public boolean isFicheroCreado() {
		return ficheroCreado;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
