package mgamallo;

import java.util.Calendar;
import java.util.TimeZone;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

public class PiePagina extends PdfPageEventHelper {

	private String encabezado;
	PdfTemplate total;

	@Override
	public void onOpenDocument(PdfWriter writer, Document document) {
		total = writer.getDirectContent().createTemplate(30, 1114);
	}

	@Override
	public void onEndPage(PdfWriter writer, Document document) {
		
		Font fuente3N = new Font(FontFamily.TIMES_ROMAN, 9, Font.BOLD);
		
		if (writer.getPageNumber() != 1) {
			try {
				PdfPTable tablaEncabrezado = new PdfPTable(Pdf.numeroTotalColumnas);
				tablaEncabrezado.setWidthPercentage(100);
				tablaEncabrezado.setHeaderRows(1);

				int numColumnas = 2 + Planificacion.horas.length + 1;
				String encabezados[] = new String[numColumnas];

				float anchoColumnas[] = new float[numColumnas];

				encabezados[0] = "C�digo";
				anchoColumnas[0] = 1.5f;
				encabezados[1] = "Descripci�n";
				anchoColumnas[1] = 4f;
				for (int i = 0; i < Planificacion.horas.length; i++) {
					encabezados[i + 2] = Planificacion.horas[i];
					anchoColumnas[i + 2] = 1f;
				}
				encabezados[numColumnas - 1] = "Total";
				anchoColumnas[numColumnas - 1] = 1f;

				tablaEncabrezado.setWidths(anchoColumnas);

				
				
				PdfPCell celdas[] = new PdfPCell[numColumnas];
				for (int i = 0; i < numColumnas; i++) {
					Paragraph contenido = new Paragraph();
					contenido.setFont(fuente3N);
					contenido.add(encabezados[i]);
					celdas[i] = new PdfPCell(contenido);
					celdas[i].setHorizontalAlignment(Element.ALIGN_CENTER);
					tablaEncabrezado.addCell(celdas[i]);
				}
				// tablaEncabrezado.writeSelectedRows(0, 1, 0, 803, writer.getDirectContent());
				
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		PdfPTable table = new PdfPTable(3);
		try {

			table.setWidths(new int[] { 24, 24, 2 });
			table.setTotalWidth(527);
			table.setLockedWidth(true);
			table.getDefaultCell().setFixedHeight(20);

			// Borde de la celda
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

			table.addCell("");
			table.addCell("");

			PdfPCell cell = new PdfPCell(Image.getInstance(total));
			cell.setBorder(Rectangle.NO_BORDER);

			table.addCell(cell);
			// Esta linea escribe la tabla como encabezado
			table.writeSelectedRows(0, 1, 60, 803, writer.getDirectContent());

		} catch (DocumentException de) {
			// TODO: handle exception
			throw new ExceptionConverter(de);
		}

		Calendar cal1 = Calendar.getInstance(TimeZone.getDefault());

		int numMes = (cal1.get(Calendar.MONTH) + 1);
		String mes = "";
		if (numMes < 10) {
			mes = "0" + numMes;
		} else {
			mes = "" + numMes;
		}

		int numMinutos = cal1.get(Calendar.MINUTE);
		String min = "";
		if (numMinutos < 10) {
			min = "0" + numMinutos;
		} else {
			min = "" + numMinutos;
		}

		int numSeg = cal1.get(Calendar.SECOND);
		String seg = "";
		if (numSeg < 10) {
			seg = "0" + numSeg;
		} else {
			seg = "" + numSeg;
		}

		String fecha = "" + cal1.get(Calendar.DATE) + "/" + mes + "/"
				+ cal1.get(Calendar.YEAR) + " "
				+ cal1.get(Calendar.HOUR_OF_DAY) + ":" + min + ":" + seg;

		String numHoja = String.valueOf(writer.getPageNumber());
		fecha = fecha + "     p�gina " + numHoja + " de";

		Font fuente2 = new Font(FontFamily.TIMES_ROMAN, 9, Font.ITALIC);
		Phrase p = new Phrase();
		p.setFont(fuente2);
		p.add(fecha);

		ColumnText.showTextAligned(writer.getDirectContent(),
				Element.ALIGN_RIGHT, p, 565, 20, 0);

		p = new Phrase();
		p.setFont(fuente2);
		String nota = "# Comentario del cliente";
		p.add(nota);

		ColumnText.showTextAligned(writer.getDirectContent(),
				Element.ALIGN_LEFT, p, 20, 20, 0);
	}

	@Override
	public void onCloseDocument(PdfWriter writer, Document document) {

		String totalHojas = String.valueOf(writer.getPageNumber() - 1);

		// Font fuente2 = new Font(FontFamily.TIMES_ROMAN, 9, Font.ITALIC);
		Phrase p = new Phrase();
		p.setFont(new Font(FontFamily.TIMES_ROMAN, 13, Font.ITALIC));
		p.add(totalHojas);

		ColumnText.showTextAligned(total, Element.ALIGN_LEFT, p, 2, 0, 0);
	}

}
