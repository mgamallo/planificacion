package mgamallo;

import java.util.Calendar;

public class Item {
	


	
	private int numPedido;
	private int numLin;
	private int codArticulo;
	private String descripcion;
	
	private int codigoSeccion;
	private String seccion;
	
	private String modificador;
	private String peso;
	
	private int unidades;
	private Calendar fechaEntrega;
	private Calendar horaEntrega;

	public int getNumPedido() {
		return numPedido;
	}
	public void setNumPedido(int numPedido) {
		this.numPedido = numPedido;
	}
	public int getNumLin() {
		return numLin;
	}
	public void setNumLin(int numLin) {
		this.numLin = numLin;
	}
	public int getCodArticulo() {
		return codArticulo;
	}
	public void setCodArticulo(int codArticulo) {
		this.codArticulo = codArticulo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getModificador() {
		return modificador;
	}
	public void setModificador(String modificador) {
		this.modificador = modificador;
	}
	public String getPeso() {
		return peso;
	}
	public void setPeso(String peso) {
		this.peso = peso;
	}
	public int getUnidades() {
		return unidades;
	}
	public void setUnidades(int unidades) {
		this.unidades = unidades;
	}
	public Calendar getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(Calendar fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	public Calendar getHoraEntrega() {
		return horaEntrega;
	}
	public void setHoraEntrega(Calendar horaEntrega) {
		this.horaEntrega = horaEntrega;
	}

	public int getCodigoSeccion() {
		return codigoSeccion;
	}
	public void setCodigoSeccion(int codigo) {
		this.codigoSeccion = codigo;
	}
	public String getSeccion() {
		return seccion;
	}
	public void setSeccion(String descripcionSeccion) {
		this.seccion = descripcionSeccion;
	}
	
}
