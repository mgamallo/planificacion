package mgamallo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.TreeSet;

import javax.swing.tree.TreeSelectionModel;

public class Seccion {

	
	private int codigo;
	private String nombre;
	private ArrayList<ArticuloSolicitado> listaArticulos
	      = new ArrayList<ArticuloSolicitado>();
	
	private ArrayList<Agregado> listaAgregados = new ArrayList<Agregado>();
	
	private int tablaContadora[][];
	
	public int[][] getTablaContadora() {
		return tablaContadora;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String descripcion) {
		this.nombre = descripcion;
	}
	
	public ArrayList<Agregado> getListaAgregados() {
		return listaAgregados;
	}

	
	public ArrayList<ArticuloSolicitado> getListaItems() {
		return listaArticulos;
	}

	
	public void addArticulo(ArticuloSolicitado articulo) {
		this.listaArticulos.add(articulo);
	}

	
	public void generarAgregados(){
		
		while(listaArticulos.size() > 0){
			Agregado agregado = new Agregado(listaArticulos.get(0));
			
			for(int i=1;i<listaArticulos.size();i++){
				if(esIgual(listaArticulos.get(0), listaArticulos.get(i))){
					agregado.add(listaArticulos.get(i));
					listaArticulos.remove(i);
					i--;
				}
			}
			
			if(agregado.getSumaArticulos() != 0){
				listaAgregados.add(agregado);
			}

			listaArticulos.remove(0);
		}
	}
	
	private boolean esIgual(ArticuloSolicitado articulo1, ArticuloSolicitado articulo2){
		
		boolean resultado = false;
		
		if(articulo1.getCodArticulo() == articulo2.getCodArticulo()){
			
			if(articulo1.getListaModificadores().size() == articulo2.getListaModificadores().size()){
				TreeSet<String> conjunto1 = new TreeSet<String>(articulo1.getListaModificadores());
				TreeSet<String> conjunto2 = new TreeSet<String>(articulo2.getListaModificadores());

				for (String element : conjunto1){
		             if(!conjunto2.contains(element)){
		            	 return false;
		             }
		          } 
				
				return true;
			}
		}
		
		
		return false;
	}
	
	public void generarTablaHoras(){
		
		int numHoras = Planificacion.horas.length;
		int numAgregados = listaAgregados.size();
		
		int horasEntero[] = new int[numHoras];
		for(int i=0;i<numHoras;i++){
			String[] arrayHora = Planificacion.horas[i].split(":");
			horasEntero[i] = Integer.valueOf(arrayHora[0] + arrayHora[1]);
		}
		
		
		tablaContadora = new int[numAgregados + 1][numHoras];
		
		for(int i=0;i<numAgregados;i++){
			for(int j=0;j<numHoras;j++){
				tablaContadora[i][j] = 0;
			}
		}
		
		for(int i=0;i<numAgregados;i++){
			for(EntregaHora entrega : listaAgregados.get(i).getHoras()){
				int numColum = celdaTabla(entrega, horasEntero);
				tablaContadora[i][numColum] += entrega.getNumEntregas(); 
			}
		}
		
		/*
		System.out.println("******************************");
		System.out.println("Seccion " + nombre);
		for(int i=0;i<numAgregados;i++){
			for(int j=0;j<numHoras;j++){
				System.out.print(tablaContadora[i][j] + "\t");
			}
			System.out.println();
		}
		System.out.println("*************************************");
		*/
	}
	
	private int celdaTabla(EntregaHora entrega, int[] horasEntero){
		
		int hora = entrega.getHora().get(Calendar.HOUR_OF_DAY)*100;
		int min = entrega.getHora().get(Calendar.MINUTE);
		
		int horaCompleta = hora+min; 
		

		
		for(int i=1;i<horasEntero.length;i++){
			
			/* Si el pedido es a las 12:00 asigna a las 11:30
			 * Si el pedido es a las 9:30 asigna a las 8:30
			 */
			if(horaCompleta <= horasEntero[i]){
				return i-1;
			}
			
/*
			if(horaCompleta == horasEntero[i]){
				return i;
			}else if(horaCompleta < horasEntero[i] ){
				return i-1;
			}
*/			
		}
		
		return horasEntero.length;
	}
}
