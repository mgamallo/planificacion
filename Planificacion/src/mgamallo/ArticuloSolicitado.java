package mgamallo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TreeSet;

public class ArticuloSolicitado {

	private int numPedido;
	private int codArticulo;
	private int numLin;
	
	private String descripcion;
	private ArrayList<String> listaModificadores = new ArrayList<String>();
	private ArrayList<Modificador> modificadores = new ArrayList<Modificador>();
	
	
	private int codSeccion;
	private String seccion;
	private int unidades;
	
	private Calendar fechaEntrega;
	private Calendar horaEntrega;
	
	public int getNumPedido() {
		return numPedido;
	}
	public void setNumPedido(int numPedido) {
		this.numPedido = numPedido;
	}
	public int getCodArticulo() {
		return codArticulo;
	}
	public void setCodArticulo(int codArticulo) {
		this.codArticulo = codArticulo;
	}
	public int getNumLin() {
		return numLin;
	}
	public void setNumLin(int numLin) {
		this.numLin = numLin;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public ArrayList<String> getListaModificadores() {
		return listaModificadores;
	}
	public void setListaModificadores(ArrayList<String> listaModificadores) {
		this.listaModificadores = listaModificadores;
	}
	
	public void addModificador(String modificador, String peso) {
		
		// this.unidades = unidades;
		boolean yaIncluido = false;
		for(int i=0;i<modificadores.size();i++){
			if(modificador.equals(modificadores.get(i).getNombre())){
				modificadores.get(i).addCantidad(1);
				yaIncluido = true;
				break;
			}
		}

		if(!yaIncluido){
			modificadores.add(new Modificador(modificador, 1));
		}
		
		listaModificadores.clear();
		for(int i=0;i<modificadores.size();i++){
			
			String mod = modificadores.get(i).getNombre();
			if(modificadores.get(i).getCantidad() > 1){
				mod = mod + (" (" + modificadores.get(i).getCantidad() + ")");
			}
			
			if(mod != null){
				listaModificadores.add(mod);
			}
			
		}
		
		
		if(!peso.equals("0") && !peso.equals("0.0")){
			this.listaModificadores.add(peso + " Kg.");
		}		
		
		
		
		
/*		
		if(modificador != null){
			
			String mod = modificador;
			
			
			if(unidades > 1){
				mod = mod + (" (" + unidades + ")");
			}
			
			
			this.listaModificadores.add(mod);
		}
*/
		System.out.println("Imprime modificadores: ");
		for(int i=0;i<listaModificadores.size();i++){
			System.out.println(listaModificadores.get(i));
		}
		
		TreeSet<String> conjuntoModificadores = new TreeSet<String>(listaModificadores);
		this.listaModificadores = new ArrayList<String>();
		if(conjuntoModificadores != null){
			this.listaModificadores.addAll(conjuntoModificadores);
		}
		
		
	}
	
	public int getCodSeccion() {
		return codSeccion;
	}
	public void setCodSeccion(int codSeccion) {
		this.codSeccion = codSeccion;
	}
	public String getSeccion() {
		return seccion;
	}
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	public int getUnidades() {
		return unidades;
	}
	public void setUnidades(int unidades) {
		this.unidades = unidades;
	}
	public Calendar getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(Calendar fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	public Calendar getHoraEntrega() {
		return horaEntrega;
	}
	public void setHoraEntrega(Calendar horaEntrega) {
		this.horaEntrega = horaEntrega;
	}
	
}
