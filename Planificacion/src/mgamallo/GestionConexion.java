package mgamallo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import encriptado.Conexion;

public class GestionConexion {

	String URL;
	String bd;
	String user;
	String pass;
	String consultaSQL = "";

	Connection con = null;
	Statement stmt = null;
	ResultSet rs = null;

	GestionConexion(String consulta) {
		this.URL = "jdbc:sqlserver://PC-PC\\sqlexpress;databaseName=";
		// this.URL = "jdbc:sqlserver://CAFETERIA-PC\\sqlexpress;databaseName=";
		// this.bd = "DEMOFRS";
		 this.bd = "FRONTREST";
		// this.bd = "CAFESRG";
		// this.bd = "SENA";
		this.user = "sa";
		this.pass = "masterkey";
		this.consultaSQL = consulta;

	}

	GestionConexion(Conexion conexion) {
		// this.URL = "jdbc:sqlserver://PC-PC\\sqlexpress;databaseName=";
		this.URL = "jdbc:sqlserver://" + conexion.getNombreServidor()
				+ ";databaseName=";
		System.out.println(URL);

		this.bd = conexion.getNombreBD();
		this.user = conexion.getNombreUsuario();
		this.pass = conexion.getClave();
		this.consultaSQL = conexion.getConsultaSQL();

	}

	boolean setConexion() {
		boolean conectado = false;

		try {
			String connectionUrl = URL + bd;

			System.out.println(connectionUrl);
			System.out.println(user);
			System.out.println(pass);

			con = DriverManager.getConnection(connectionUrl, user, pass);
			System.out.println("Conectado.");
			conectado = true;
		} catch (SQLException ex) {
			System.out.println("Error al conectar.");
		}

		return conectado;
	}

	boolean cerrarConexion() {
		boolean cerrada = false;
		try {
			if (con != null && !con.isClosed()) {
				con.close();
				System.out.println("Conexion cerrada");
				cerrada = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cerrada;
	}

	ResultSet ejecutaConsulta(String consulta) {

		ResultSet resultado = null;
		try {
			stmt = con.createStatement();
			resultado = stmt.executeQuery(consulta);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resultado;
	}

	void mostrarConsulta() {

		SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		formateador.setLenient(false);
		
		if (rs != null) {
			try {
				int i = 1;
				while (rs.next()) {
					
					Calendar cal = Calendar.getInstance();
					

					try {
						cal.setTime(formateador.parse(rs.getString("HORA")));
						// System.out.println(cal.getTime());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

										
					System.out.println(i + "\t" + rs.getInt(1) + "\t" + rs.getInt(2) + "\t" + rs.getInt(3)
							+ "\t" + rs.getString(4) + "\t" + rs.getInt(5) + "\t" + rs.getString(6)
							+ "\t" + rs.getString(7) + "\t" + rs.getString(8) + "\t" + rs.getInt(9)
							+ "\t" + rs.getString(10) + "\t" + rs.getString(11) 
							+ "\t" + cal.getTime()
							
							);
					i++;
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/*
		 * String SQL = "" +
		 * "SELECT M1.ORDEN, M2.DESCRIPCION AS DESCRIPCION_ORDEN, A.DESCRIPCION "
		 * + "FROM MODIFICADORESARTICULOS M1 " +
		 * "LEFT JOIN MODIFICADORESCAB M2 ON (M1.CODMODIFICADOR = M2.CODMODIFICADOR) "
		 * +
		 * "LEFT JOIN MODIFICADORESLIN M3 ON (M2.CODMODIFICADOR = M3.CODMODIFICADOR) "
		 * + "LEFT JOIN ARTICULOS A ON (M3.CODARTICULOCOM = A.CODARTICULO) " +
		 * "WHERE M1.CODARTICULO = 10051 " + "ORDER BY ORDEN;";
		 */

		String SQL = ""
				+ "select l.numpedido, l.numlin, l.CODARTICULO, l.DESCRIPCION, s.SECCION as codseccion, "
				+ "s.DESCRIPCION as seccion, m.DESCRIPCION as Modificador, l.PESOUNITARIO, l.UNIDADESPEN, "
				+ "c.FECHAENTREGA, c.HORAENTREGA as HORA "
				+ "from PEDVENTALIN l "
				+ "inner join pedventacab c on l.numpedido = c.numpedido "
				+ "inner join ARTICULOS a on l.CODARTICULO = a.CODARTICULO "
				+ "inner join secciones s on a.SECCION = s.SECCION "
				+ "left join PEDVENTAMODIF m on (l.numlin = m.NUMLINEA and l.NUMPEDIDO = m.NUMPEDIDO) "
				+ "where c.FECHAENTREGA between '20171130 00:00:00' and '20171130 23:59:59' "
				+ "order by numpedido;"

				+ "";

		GestionConexion conexion = new GestionConexion(SQL);

		try {
			if (conexion.setConexion()) {
				conexion.rs = conexion.ejecutaConsulta(SQL);
				// conexion.mostrarConsulta();
				
				Planificacion planificacion = new Planificacion(conexion.rs);
				new Pdf(planificacion);
				
				String fichero = new String("pdfs\\pdf0.pdf");
				
				Runtime.getRuntime().exec("cmd /c start " + fichero);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			conexion.cerrarConexion();
		}

	}

}
