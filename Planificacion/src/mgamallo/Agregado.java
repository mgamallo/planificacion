package mgamallo;

import java.util.ArrayList;

public class Agregado {

	private ArticuloSolicitado articuloAgregado;
	private ArrayList<EntregaHora> horas 
	       = new ArrayList<EntregaHora>(); 
	private int sumaArticulos = 0;
	
	public Agregado(ArticuloSolicitado articulo){
		
		this.articuloAgregado = articulo;
		sumaArticulos += articulo.getUnidades();
		
		horas.add(new EntregaHora(articulo));
		
	}
	
	public void add(ArticuloSolicitado articulo){
		
		sumaArticulos += articulo.getUnidades();
		EntregaHora hora = new EntregaHora(articulo);
		
		boolean encontrado = false;
		
		for(int i=0;i<horas.size();i++){
			if(horas.get(i).getHora() == hora.getHora()){
				horas.get(i).addNumEntregas(articulo.getUnidades());
				encontrado = true;
			}
		}
		
		if(encontrado == false){
			horas.add(hora);
		}
	}

	
	public ArticuloSolicitado getArticuloAgregado() {
		return articuloAgregado;
	}

	
	public ArrayList<EntregaHora> getHoras() {
		return horas;
	}

	
	public int getSumaArticulos() {
		return sumaArticulos;
	}
}
