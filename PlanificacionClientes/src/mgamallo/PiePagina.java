package mgamallo;

import java.util.Calendar;
import java.util.TimeZone;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

public class PiePagina extends PdfPageEventHelper{
	
	
	PdfTemplate total;
	 
	@Override
	public void onOpenDocument(PdfWriter writer, Document document) {
		total = writer.getDirectContent().createTemplate(100, 160);
	}
	

	@Override
	 public void onEndPage(PdfWriter writer, Document document) {
		
		Calendar cal1 = Calendar.getInstance(TimeZone.getDefault());

		int numMes = (cal1.get(Calendar.MONTH)+1);
		String mes = "";
		if(numMes < 10){
			mes = "0" + numMes;
		}
		else{
			mes = "" + numMes;
		}
		
		int numMinutos = cal1.get(Calendar.MINUTE);
		String min = "";
		if(numMinutos < 10){
			min = "0" + numMinutos;
		}
		else{
			min = "" + numMinutos;
		}
		
		int numSeg = cal1.get(Calendar.SECOND);
		String seg = "";
		if(numSeg < 10){
			seg = "0" + numSeg;
		}
		else{
			seg = "" + numSeg;
		}		
		
	    String fecha = ""+cal1.get(Calendar.DATE)+"/"+ mes
	    	    +"/"+cal1.get(Calendar.YEAR)+" "+cal1.get(Calendar.HOUR_OF_DAY)
	    	    +":"+ min +":"+ seg;
	    
	    String numHoja = String.valueOf(writer.getPageNumber());
	    fecha = fecha + "     " + numHoja;
		
	    Font fuente2 = new Font(FontFamily.TIMES_ROMAN, 9, Font.ITALIC);
	    Phrase p = new Phrase();
	    p.setFont(fuente2);
	    p.add(fecha);
	    
	  ColumnText.showTextAligned( writer.getDirectContent() , Element.ALIGN_RIGHT, p, 820,20,0);
	  ColumnText.showTextAligned( writer.getDirectContent() , Element.ALIGN_RIGHT, p, 530,20,0);
	  
	  p = new Phrase();
	  p.setFont(fuente2);
	  String nota = "# Comentario del cliente";
	  p.add(nota);
	  
	  ColumnText.showTextAligned( writer.getDirectContent() , Element.ALIGN_LEFT, p, 20,20,0);			
	 }

	
	@Override
	 public void onCloseDocument(PdfWriter writer, Document document) {
	    
	    String totalHojas = String.valueOf(writer.getPageNumber() - 1);
		
	   // Font fuente2 = new Font(FontFamily.TIMES_ROMAN, 9, Font.ITALIC);
	    Phrase p = new Phrase();
	   // p.setFont(fuente2);
	    String nomesale = "no me sale....";
	    p.add(totalHojas);
	    
	  ColumnText.showTextAligned(total, Element.ALIGN_LEFT, p, 80,80,0);
	 }
	 
}

