package mgamallo;

import java.util.ArrayList;

public class Cliente {

	private int codigoCliente;
	private String nombreCliente;
	private String nombreComercial;
	
	private Planificacion planificacion;
	private ArrayList<Item> listaItems = new ArrayList<Item>(); 
	
	public int getCodigoCliente() {
		return codigoCliente;
	}

	public void setCodigoCliente(int codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public Planificacion getPlanificacion() {
		return planificacion;
	}

	public void setPlanificacion(Planificacion planificacion) {
		this.planificacion = planificacion;
	}

	
	public void addItem(Item item){
		listaItems.add(item);
	}
	
	public ArrayList<Item> getItems(){
		return listaItems;
	}
}
