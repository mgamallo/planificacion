package mgamallo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.UnsupportedLookAndFeelException;

import org.freixas.jcalendar.JCalendar;
import org.freixas.jcalendar.JCalendarCombo;

import encriptado.Conexion;
import encriptado.GestionObjeto;

public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager
				.getInstalledLookAndFeels()) {
			if ("Nimbus".equals(info.getName())) {
				try {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
				} catch (ClassNotFoundException | InstantiationException
						| IllegalAccessException
						| UnsupportedLookAndFeelException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			}
		}

		Calendar fechaEntrega = menuFecha();
		String mesCad = "";
		String diaCad = "";

		int anho = fechaEntrega.get(Calendar.YEAR);
		int mes = fechaEntrega.get(Calendar.MONTH) + 1;

		if (mes < 10) {
			mesCad = "0" + mes;
		} else {
			mesCad = "" + mes;
		}

		int dia = fechaEntrega.get(Calendar.DAY_OF_MONTH);

		if (dia < 10) {
			diaCad = "0" + dia;
		} else {
			diaCad = "" + dia;
		}

		String fechaInicial = "" + anho + mesCad + diaCad + " 00:00:00";
		String fechaFinal = "" + anho + mesCad + diaCad + " 23:59:59";

		System.out.println(fechaInicial);
		System.out.println(fechaFinal);

		System.out.println();

		String SQL = ""
				+ "select l.numpedido, l.numlin, l.CODARTICULO, l.DESCRIPCION, s.SECCION as codseccion, "
				+ "s.DESCRIPCION as seccion, m.DESCRIPCION as Modificador, l.PESOUNITARIO, l.UNIDADESPEN, "
				+ "c.FECHAENTREGA, c.HORAENTREGA as HORA, "
				+ "c.CODCLIENTE, cl.NOMBRECLIENTE, cl.NOMBRECOMERCIAL "
				+ "from PEDVENTALIN l "
				+ "inner join pedventacab c on l.numpedido = c.numpedido "
				+ "inner join ARTICULOS a on l.CODARTICULO = a.CODARTICULO "
				+ "inner join secciones s on a.SECCION = s.SECCION "
				+ "left join PEDVENTAMODIF m on (l.numlin = m.NUMLINEA and l.NUMPEDIDO = m.NUMPEDIDO) "
				+ "inner join CLIENTES cl on c.CODCLIENTE = cl.CODCLIENTE "
				+ "where c.TIPORESERVA != 4 and c.FECHAENTREGA between '" + fechaInicial + "' and '"
				+ fechaFinal + "' order by l.descripcion;"

				+ "";

		Conexion conexion = GestionObjeto.deserializaObjeto();

		/* Desencriptamos los atributos del objeto conexion */

		if (conexion.desEncriptaObjeto()) {
			GestionConexion gc = new GestionConexion(conexion);
			gc.setConexion();

			// GestionConexion conexion = new GestionConexion(SQL);

		/*
			SQL = conexion.getConsultaSQL() + fechaInicial + "' and '"
					+ fechaFinal + "' order by l.descripcion;" ;
		*/

			
			try {
				if (gc.setConexion()) {
					gc.rs = gc.ejecutaConsulta(SQL);
					// conexion.mostrarConsulta();

					PlanificacionClientes planificacionClientes = new PlanificacionClientes(gc.rs);
					/*
					Pdf pdf = new Pdf(planificacionClientes);
					System.out.println(pdf.isFicheroCreado());

					if (pdf.isFicheroCreado()) {
						String fichero = new String("pdfs\\pdf0.pdf");

						Runtime.getRuntime().exec("cmd /c start " + fichero);
					}
					*/
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				gc.cerrarConexion();
			}
		} else {
			Error.publicaError("Error al desencriptar los parámetros de la conexion");
		}

	}

	private static Calendar menuFecha() {

		// JCalendar calendario = new JCalendar();
		final JCalendarCombo calEjemplo02 = new JCalendarCombo();

		String message = "Selecciona la fecha de entrega:\n\n";
		String saltoLinea = "\n";
		Object[] params = { message, /* calendario, */calEjemplo02, saltoLinea };
		int resultado = JOptionPane.showConfirmDialog(null, params,
				"Imprimir planificación", JOptionPane.OK_CANCEL_OPTION);

		if (resultado == JOptionPane.OK_OPTION) {
			Date fecha = calEjemplo02.getDate();
			Calendar calendario = Calendar.getInstance();
			calendario.setTime(fecha);

			return calendario;
		}

		return null;
	}
}
