package mgamallo;

import java.util.Calendar;

public class EntregaHora {

	private Calendar hora;
	private int numEntregas = 0;
	
	public EntregaHora(ArticuloSolicitado articulo){
		this.hora = articulo.getHoraEntrega();
		numEntregas = articulo.getUnidades();
	}

	public Calendar getHora() {
		return hora;
	}

	public void setHora(Calendar hora) {
		this.hora = hora;
	}

	public int getNumEntregas() {
		return numEntregas;
	}

	public void addNumEntregas(int numEntregas) {
		this.numEntregas += numEntregas;
	}
}
