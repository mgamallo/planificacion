package mgamallo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.TreeSet;

import javax.swing.JOptionPane;

public class Planificacion {

	static public String horas[] = {"07:30", "08:30", "09:30", "10:30", "11:30", "12:30", "13:30", "14:30",
									"17:30", "18:30", "19:30", "20:30"};
	
	private Calendar fechaEntrega;
	private ArrayList<ArticuloSolicitado> listaArticulos = new ArrayList<ArticuloSolicitado>();
	private ArrayList<String> estadosReserva = new ArrayList<String>();

	private ArrayList<Seccion> listaSecciones = new ArrayList<Seccion>();

	ArrayList<Item> items = new ArrayList<Item>();

	public Planificacion(Cliente cliente){
		generarArticulos(cliente.getItems());
		generarSecciones(listaArticulos);
		
		System.out.println("**** PLANIFICACI�N DE " + cliente.getNombreCliente());
	    System.out.println("*****************************************");
		for(int i=0;i<listaSecciones.size();i++){
			listaSecciones.get(i).generarAgregados();
			System.out.println("Nombre secci�n: " + listaSecciones.get(i).getNombre() 
					+ ". N� de Agregados:" + listaSecciones.get(i).getListaAgregados().size());
			System.out.println("\t\t**********************************");
			for(int j=0;j<listaSecciones.get(i).getListaAgregados().size();j++){
				System.out.println(
						listaSecciones.get(i).getListaAgregados().get(j).getArticuloAgregado().getDescripcion() +
						"\t\t" + listaSecciones.get(i).getListaAgregados().get(j).getSumaArticulos());
				System.out.println("\t\t\t\t*******************************");
				for(int z = 0;z<listaSecciones.get(i).getListaAgregados().get(j).getHoras().size();z++){
					System.out.println("\t\t\t\t" + listaSecciones.get(i).getListaAgregados().get(j).getHoras().get(z).getHora().getTime());
				}
			}
		System.out.println("*****************************************");
		}
		
		System.out.println("Fin de agregados");
	/*	
		System.out.println();
		System.out.println("Tablas de horas");
		for(int i=0;i<horas.length;i++){
			System.out.print(horas[i] + "\t");
		}
		System.out.println();
		*/
		for(int i=0;i<listaSecciones.size();i++){
			listaSecciones.get(i).generarTablaHoras();
		}		
		
	}

	
	
	public Planificacion(ResultSet resultadoSQL) {

		boolean error = cargaDatosSQL(resultadoSQL);

		if (error) {
			System.out.println("Error al crear el objeto.");
			JOptionPane.showMessageDialog(null, "Error al conectar a la base de datos");
		} else {
			System.out.println("Objeto creado");
			System.out.println(fechaEntrega.getTime());
		}

		
		 for (Item item : items) { System.out.println(item.getNumPedido() +
		 "\t" + item.getHoraEntrega().getTime() + " ---- " +
		 item.getHoraEntrega().getTime().getMinutes()); }
		 

		generarArticulos(items);
		generarSecciones(listaArticulos);
		
		
		
	    System.out.println("*****************************************");
		for(int i=0;i<listaSecciones.size();i++){
			listaSecciones.get(i).generarAgregados();
			System.out.println("Nombre secci�n: " + listaSecciones.get(i).getNombre() 
					+ ". N� de Agregados:" + listaSecciones.get(i).getListaAgregados().size());
			System.out.println("\t\t**********************************");
			for(int j=0;j<listaSecciones.get(i).getListaAgregados().size();j++){
				System.out.println(
						listaSecciones.get(i).getListaAgregados().get(j).getArticuloAgregado().getDescripcion() +
						"\t\t" + listaSecciones.get(i).getListaAgregados().get(j).getSumaArticulos());
				System.out.println("\t\t\t\t*******************************");
				for(int z = 0;z<listaSecciones.get(i).getListaAgregados().get(j).getHoras().size();z++){
					System.out.println("\t\t\t\t" + listaSecciones.get(i).getListaAgregados().get(j).getHoras().get(z).getHora().getTime());
				}
			}
		System.out.println("*****************************************");
		}
		
		System.out.println("Fin de agregados");
	/*	
		System.out.println();
		System.out.println("Tablas de horas");
		for(int i=0;i<horas.length;i++){
			System.out.print(horas[i] + "\t");
		}
		System.out.println();
		*/
		for(int i=0;i<listaSecciones.size();i++){
			listaSecciones.get(i).generarTablaHoras();
		}
	}


	public Calendar getFechaEntrega() {
		return fechaEntrega;
	}


	public void setFechaEntrega(Calendar fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}


	public ArrayList<ArticuloSolicitado> getListaArticulos() {
		return listaArticulos;
	}


	public void setListaArticulos(ArrayList<ArticuloSolicitado> listaArticulos) {
		this.listaArticulos = listaArticulos;
	}


	public ArrayList<String> getEstadosReserva() {
		return estadosReserva;
	}


	public void setEstadosReserva(ArrayList<String> estadosReserva) {
		this.estadosReserva = estadosReserva;
	}


	public ArrayList<Seccion> getListaSecciones() {
		return listaSecciones;
	}


	public void setListaSecciones(ArrayList<Seccion> listaSecciones) {
		this.listaSecciones = listaSecciones;
	}


	public void imprimePlanificacion(){
		System.out.println();
	}
	
	private boolean cargaDatosSQL(ResultSet resultado) {

		boolean error = false;

		SimpleDateFormat formateador = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss.SSS");
		formateador.setLenient(false);


		
		int i = 0;
		if (resultado != null) {
			try {
				while (resultado.next()) {

					/*
					 * En principio esta variable la recogemos de un
					 * joptionpane.
					 */
					if (i == 0) {

						fechaEntrega = Calendar.getInstance();
						try {
							fechaEntrega.setTime(formateador.parse(resultado
									.getString("FECHAENTREGA")));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
					/************************************************************/

					Item item = new Item();

					item.setNumPedido(resultado.getInt("numpedido"));
					item.setNumLin(resultado.getInt("numlin"));
					item.setCodArticulo(resultado.getInt("CODARTICULO"));
					item.setDescripcion(resultado.getString("DESCRIPCION"));
					item.setCodigoSeccion(resultado.getInt("codseccion"));
					item.setSeccion(resultado.getString("seccion"));
					item.setModificador(resultado.getString("Modificador"));
					item.setPeso(resultado.getString("PESOUNITARIO"));
					item.setUnidades(resultado.getInt("UNIDADESPEN"));

					/* fechas */
					Calendar cal = Calendar.getInstance();
					try {
						cal.setTime(formateador.parse(resultado
								.getString("FECHAENTREGA")));
						item.setFechaEntrega(cal);

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						item.setFechaEntrega(null);
					}

					/* fechas */
					cal = Calendar.getInstance();
					try {
						cal.setTime(formateador.parse(resultado
								.getString("HORA")));
						item.setHoraEntrega(cal);

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						item.setHoraEntrega(null);
					}

					items.add(item);
					i++;
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				error = true;
			}
			System.out.println("N�mero de filas: " + i);
			if(i == 0){
				JOptionPane.showMessageDialog(null, "No hay resultados para esta fecha");
			}
		}

		return error;
	}

	private void generarArticulos(ArrayList<Item> listaItems) {

		int numPedidoAnterior = 0;
		int numCodArticuloAnterior = 0;
		int numLinAnterior = 0;
		ArticuloSolicitado articulo = new ArticuloSolicitado();

		for (int i = 0; i < listaItems.size(); i++) {
			if (i == 0) {
				articulo.setNumPedido(listaItems.get(i).getNumPedido());
				numPedidoAnterior = listaItems.get(i).getNumPedido();
				articulo.setCodArticulo(listaItems.get(i).getCodArticulo());
				numCodArticuloAnterior = listaItems.get(i).getCodArticulo();
				articulo.setNumLin(listaItems.get(i).getNumLin());
				numLinAnterior = listaItems.get(i).getNumLin();

				articulo.setDescripcion(listaItems.get(i).getDescripcion());
				articulo.setCodSeccion(listaItems.get(i).getCodigoSeccion());
				articulo.setSeccion(listaItems.get(i).getSeccion());

				articulo.addModificador(listaItems.get(i).getModificador(),
						listaItems.get(i).getPeso());

				articulo.setUnidades(listaItems.get(i).getUnidades());
				articulo.setFechaEntrega(listaItems.get(i).getFechaEntrega());
				articulo.setHoraEntrega(listaItems.get(i).getHoraEntrega());

			} else {
				if (numPedidoAnterior == listaItems.get(i).getNumPedido()
						&& numCodArticuloAnterior == listaItems.get(i)
								.getCodArticulo()
						&& numLinAnterior == listaItems.get(i).getNumLin()) {

					articulo.addModificador(listaItems.get(i).getModificador(),
							listaItems.get(i).getPeso());
				} else {

					listaArticulos.add(articulo);

					articulo = new ArticuloSolicitado();

					articulo.setNumPedido(listaItems.get(i).getNumPedido());
					numPedidoAnterior = listaItems.get(i).getNumPedido();
					articulo.setCodArticulo(listaItems.get(i).getCodArticulo());
					numCodArticuloAnterior = listaItems.get(i).getCodArticulo();
					articulo.setNumLin(listaItems.get(i).getNumLin());
					numLinAnterior = listaItems.get(i).getNumLin();

					articulo.setDescripcion(listaItems.get(i).getDescripcion());
					articulo.setCodSeccion(listaItems.get(i).getCodigoSeccion());
					articulo.setSeccion(listaItems.get(i).getSeccion());

					articulo.addModificador(listaItems.get(i).getModificador(),
							listaItems.get(i).getPeso());

					articulo.setUnidades(listaItems.get(i).getUnidades());
					articulo.setFechaEntrega(listaItems.get(i)
							.getFechaEntrega());
					articulo.setHoraEntrega(listaItems.get(i).getHoraEntrega());
				}
			}
		}
		
		
		listaArticulos.add(articulo);

		System.out.println("Lista de items: " + listaItems.size());
		System.out.println("Lista de articulos: " + listaArticulos.size());

		for (ArticuloSolicitado art : listaArticulos) {

			System.out
					.print(art.getNumPedido() + "\t" + art.getCodArticulo()
							+ "\t" + art.getDescripcion() + "\t"
							+ art.getNumLin() + "\t"
							+ art.getFechaEntrega().getTime() + "\t"
							+ art.getHoraEntrega().getTime() + "\t"
							+ art.getUnidades());

			for (int i = 0; i < art.getListaModificadores().size(); i++) {
				System.out.print("\n \t \t \t \t ");
				System.out.print(art.getListaModificadores().get(i));
			}
			System.out.println();
		}
	}

	private void generarSecciones(ArrayList<ArticuloSolicitado> listaArt) {

		TreeSet<Integer> conjuntoSecciones = new TreeSet<Integer>(
				Collections.reverseOrder());

		for (int i = 0; i < listaArticulos.size(); i++) {
			conjuntoSecciones.add(listaArticulos.get(i).getCodSeccion());
		}

		for (Iterator it = conjuntoSecciones.iterator(); it.hasNext();) {

			int x = (int) it.next();
			/* System.out.println(x); */

			Seccion seccion = new Seccion();
			for (int i = 0, z = 0; i < listaArticulos.size(); i++) {
				if (x == listaArticulos.get(i).getCodSeccion()) {
					if (z == 0) {
						seccion.setCodigo(x);
						seccion.setNombre(listaArticulos.get(i).getSeccion());
						z++;
					}

					seccion.addArticulo(listaArticulos.get(i));
				}

			}
			listaSecciones.add(seccion);

		}

		System.out.println("Lista Secciones *******************************************");
		for (int i = 0; i < listaSecciones.size(); i++) {
			System.out.println(listaSecciones.get(i).getCodigo()
					+ "\t" + listaSecciones.get(i).getNombre()
					+ "\t\t" + listaSecciones.get(i).getListaItems().size());
		}
	}
	
}
