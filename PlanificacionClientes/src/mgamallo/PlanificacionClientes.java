package mgamallo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.TreeSet;

import javax.swing.JOptionPane;

public class PlanificacionClientes {

	private Calendar fechaEntrega;

	ArrayList<Item> items = new ArrayList<Item>();
	ArrayList<Cliente> clientes = new ArrayList<Cliente>();	
	
	public PlanificacionClientes(ResultSet resultadoSQL) {

		boolean error = cargaDatosSQL(resultadoSQL);

		if (error) {
			System.out.println("Error al crear el objeto PlanificacionClientes.");
			JOptionPane.showMessageDialog(null, "Error al conectar a la base de datos");
		} else {
			System.out.println("Objeto creado");
			// System.out.println(fechaEntrega.getTime());
		}
		
		/*
		 for (Item item : items) { System.out.println(item.getNumPedido() +
		 "\t" + item.getHoraEntrega().getTime() + " ---- " +
		 item.getHoraEntrega().getTime().getMinutes()); }
		 */
		
		 generarClientes(items);
		 
		 for(int i=0;i<clientes.size();i++){
			 clientes.get(i).setPlanificacion(new Planificacion(clientes.get(i)));
		 }
	}
	
	public Calendar getFechaEntrega() {
		return fechaEntrega;
	}


	public void setFechaEntrega(Calendar fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	
	private boolean cargaDatosSQL(ResultSet resultado) {

		boolean error = false;

		SimpleDateFormat formateador = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss.SSS");
		formateador.setLenient(false);


		
		int i = 0;
		if (resultado != null) {
			try {
				while (resultado.next()) {

					/*
					 * En principio esta variable la recogemos de un
					 * joptionpane.
					 */
					if (i == 0) {

						fechaEntrega = Calendar.getInstance();
						try {
							fechaEntrega.setTime(formateador.parse(resultado
									.getString("FECHAENTREGA")));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
					/************************************************************/

					Item item = new Item();

					item.setNumPedido(resultado.getInt("numpedido"));
					item.setNumLin(resultado.getInt("numlin"));
					item.setCodArticulo(resultado.getInt("CODARTICULO"));
					item.setDescripcion(resultado.getString("DESCRIPCION"));
					item.setCodigoSeccion(resultado.getInt("codseccion"));
					item.setSeccion(resultado.getString("seccion"));
					item.setModificador(resultado.getString("Modificador"));
					item.setPeso(resultado.getString("PESOUNITARIO"));
					item.setUnidades(resultado.getInt("UNIDADESPEN"));
					item.setCodCliente(resultado.getInt("CODCLIENTE"));
					item.setNombreCliente(resultado.getString("NOMBRECLIENTE"));
					item.setNombreComercial(resultado.getString("NOMBRECOMERCIAL"));

					/* fechas */
					Calendar cal = Calendar.getInstance();
					try {
						cal.setTime(formateador.parse(resultado
								.getString("FECHAENTREGA")));
						item.setFechaEntrega(cal);

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						item.setFechaEntrega(null);
					}

					/* fechas */
					cal = Calendar.getInstance();
					try {
						cal.setTime(formateador.parse(resultado
								.getString("HORA")));
						item.setHoraEntrega(cal);

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						item.setHoraEntrega(null);
					}

					items.add(item);
					i++;
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				error = true;
			}
			System.out.println("N�mero de filas: " + i);
			if(i == 0){
				JOptionPane.showMessageDialog(null, "No hay resultados para esta fecha");
			}
		}

		return error;
	}

	private void generarClientes(ArrayList<Item> listaItems){
		
		TreeSet<Integer> conjuntoClientes = new TreeSet<Integer>(
				Collections.reverseOrder());
	
		for (int i = 0; i < listaItems.size(); i++) {
			conjuntoClientes.add(listaItems.get(i).getCodCliente());
		}
		
		for (Iterator it = conjuntoClientes.iterator(); it.hasNext();) {

			int x = (int) it.next();
			/* System.out.println(x); */

			Cliente cliente = new Cliente();
			for (int i = 0, z = 0; i < listaItems.size(); i++) {
				if (x == listaItems.get(i).getCodCliente()) {
					if (z == 0) {
						cliente.setCodigoCliente(x);
						cliente.setNombreCliente(listaItems.get(i).getNombreCliente());
						z++;
					}

					cliente.addItem(listaItems.get(i));
				}

			}
			
			clientes.add(cliente);
		}
		
		System.out.println("Lista Clientes *******************************************");
		for (int i = 0; i < clientes.size(); i++) {
			System.out.println(clientes.get(i).getCodigoCliente()
					+ "\t" + clientes.get(i).getNombreCliente()
					+ "\t" + clientes.get(i).getNombreComercial()
					+ "\t\t" + clientes.get(i).getItems().size());
		}
	}
}
