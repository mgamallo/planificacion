package mgamallo;

public class Modificador {

	private String nombre;
	private int cantidad = 0;
	
	public Modificador(String nombre, int cantidad) {
		super();
		this.nombre = nombre;
		this.cantidad = cantidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void addCantidad(int cantidad) {
		this.cantidad += cantidad;
	}
	
	
}
